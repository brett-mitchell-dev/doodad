
# `doodad`

> Who's you're doodaddy?

`doodad` is an exploration of prototypal inheritance within shell scripting. Objects are modeled as directories, while properties and functions are modeled as files within those directories.

Each `doodad` has a set of special paths within it which are used for other special functions. `<my-doodad-dir>/.doodad-proto` stores the prototypal parent of any given doodad. When asked to retrieve a file from a doodad, `doodad` will try to find that filepath at the closest point up the `proto` ancestry.  

A single `doodad` may have more than one parent. The inheritance linearization technique used by `doodad` is fairly simple, and is shared by languages such as Scala, Ruby, and Perl. `doodad` resolves references using a right-first (or rather, bottom-first), depth-first search through the references stored in `.doodad-proto`. The first ancestor found to contain the requested item will be used.

See the [Wikipedia page on multiple inheritance](https://en.wikipedia.org/wiki/Multiple_inheritance) for a more in-depth discussion on the topic.

<!-- ??? -->
<!-- Doodads are not exactly the same as objects from JavaScript. Creating a doodad works purely off of template copying. To create a prototype from which other doodads should inherit, just create a doodad, fill it out, then use it as a template. Any subsequent updates to the parent doodad will propagate to the children by virtue of the `proto` chain. -->
<!-- ??? -->

`doodad` offers a couple of core pieces of functionality:
- `doodad new`: Creates a new doodad. Can accept a template, 
- `doodad del`: Deletes a doodad
- `doodad val`: Gets or sets a value within a doodad
- `doodad run`: Calls an executable script within a doodad. Sets '$self' to the current doodad for polymorphic behavior support.
- `doodad ls`: Lists names of doodads

---

Example doodad: Jira ticket

Create a doodad to represent a ticket. Note the use of a slash in the name to namespace the doodad. This will nest the `ticket` doodad in the `jira` folder inside your data directory, resulting in a doodad named `jira/ticket`. Note that this means if you then create a doodad named `jira`, that top-level doodad will _contain_ any namespaced doodads within it.  

```sh
$ doodad new jira/ticket
Created!
$ cd `doodad show jira/ticket`
$ tree
.
└── .doodad-proto
```

Let's add some things to this doodad that might be useful for all tickets:
1. Let's add a static value called `type` which will be set to `jira-ticket` for all jira tickets. (This is a little silly, and not very useful, but it'll help demonstrate how values are accessed in doodads)
2. Let's also add an executable script which can get the status of a ticket (we'll see how to apply this to a ticket later)

First, the static value:
```sh
$ cd `doodad show jira/ticket`
$ printf 'jira-ticket' > ./type
$ cat type
jira-ticket
```

Second, the script:

Let's assume the following can be used to get the status field of a Jira ticket:
```sh
#!/usr/bin/env bash

function main {
  local issueKey=`doodad get "$self" issue_key`

  jira-api get "/issues/$issueKey?short=true"
    | jq -r '.fields.status'
}

main "$@"
```

Let's place this script inside the `jira/ticket` doodad, and mark it as executable (assume `$MY_SCRIPT` contains the text we laid out above):

```sh
$ printf "$MY_SCRIPT" | doodad set jira/ticket get_status
$ doodad run jira/ticket get_status
No such member 'issue_key' in 'jira/ticket'
```

Now let's make a doodad to represent a specific Jira ticket.

```sh
$ doodad new jira/TCKT-123 --protos jira/ticket
Created!
$ cd `doodad show jira/ticket`
$ cat .doodad-proto
jira/ticket
```

We now have a link between the prototype (`jira/ticket`) and the child (`jira/TCKT-123`). We can now access the `type` value on this new doodad:
```sh
$ doodad get jira/TCKT-123 type
jira-ticket
```

Additionally, we can call `get_status` on `jira/TCKT-123` (after we set the `issue_key` value, that is):
```sh
$ doodad run jira/TCKT-123 get_status
No such member 'issue_key' in 'jira/ticket'
$ doodad val jira/TCKT-123 issue_key 'TCKT-123' # <- Note the alternate style for setting a value
$ doodad run jira/TCKT-123 get_status
In Progress
```

